# CSDC-MSO Documentation

This is a static html documentation repo. 

This repository auto-deploys to https://nsf-noirlab.gitlab.io/csdc/csdc-mso-docs/ with any change to the main branch.

## Quickstart git commands

- You need a [github.com](https://gitlab.com/users/sign_up) account


- If you've set up [ssh keys](https://docs.gitlab.com/ee/user/ssh.html):
```
git clone git@gitlab.com:nsf-noirlab/csdc/csdc-mso-docs.git
```
- otherwise you can clone the repo with https:
```
git clone git@gitlab.com:nsf-noirlab/csdc/csdc-mso-docs.git
```

## Workflow steps, follow these every time you make changes

```
cd csdc-mdo-docs
```

- before doing work, always remember to pull
```
git pull
```

- make your edits

- add all changes
```
git add .
```

- commit the changes
```
git commit -m 'some description of the changes'
```

- push the changes
```
git push origin main
```

