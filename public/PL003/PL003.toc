\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Pipeline Context and Datasets}{4}
\contentsline {section}{\numberline {3}Calibration Manager}{5}
\contentsline {section}{\numberline {4}Remediation}{6}
\contentsline {section}{\numberline {5}Cross-talk}{7}
\contentsline {section}{\numberline {6}Read Out Bias}{8}
\contentsline {section}{\numberline {7}Pixel Masks}{10}
\contentsline {section}{\numberline {8}Saturation}{11}
\contentsline {subsection}{\numberline {8.1}Saturation in Bias, Dome Flat, and Twilight Flat Calibrations}{11}
\contentsline {subsection}{\numberline {8.2}Saturation in Science Exposures}{11}
\contentsline {section}{\numberline {9}Bleed Trails}{11}
\contentsline {section}{\numberline {10}Bad Pixel Interpolation}{12}
\contentsline {section}{\numberline {11}Bias and Dark Count Pattern}{13}
\contentsline {subsection}{\numberline {11.1}Bias Calibrations}{13}
\contentsline {section}{\numberline {12}Dome Flat Fielding}{14}
\contentsline {subsection}{\numberline {12.1}Dome and Twilight Flat Calibrations}{14}
\contentsline {section}{\numberline {13}Data Units}{15}
\contentsline {section}{\numberline {14}Astrometric}{16}
\contentsline {subsection}{\numberline {14.1}Photometric Calibration}{17}
\contentsline {subsection}{\numberline {14.2}Astrometric Calibration}{18}
\contentsline {section}{\numberline {15}Dark Sky Self-Calibrations}{19}
\contentsline {subsection}{\numberline {15.1}The Approach}{19}
\contentsline {subsection}{\numberline {15.2}General Considerations in the Dark Sky Self-Calibrations}{20}
\contentsline {subsection}{\numberline {15.3}Identifying Exposures for Dark Sky Calibrations}{21}
\contentsline {subsection}{\numberline {15.4}Pupil Ghost and Fringe Pattern Subtraction}{25}
\contentsline {subsection}{\numberline {15.5}Dark Sky Flat}{27}
\contentsline {section}{\numberline {16}Photometric Characterization}{28}
\contentsline {section}{\numberline {17}Astrometric Transformations}{29}
\contentsline {subsection}{\numberline {17.1}Setting a Common Sampling}{29}
\contentsline {subsection}{\numberline {17.2}Resampling Images}{30}
\contentsline {section}{\numberline {18}Mosaicking}{31}
\contentsline {section}{\numberline {19}Overlap Stacking}{32}
\contentsline {subsection}{\numberline {19.1}Missing vs Bad Data}{32}
\contentsline {section}{\numberline {20}Photometric Uniformity}{36}
\contentsline {section}{\numberline {21}Data Quality Characterization}{36}
\contentsline {section}{\numberline {22}Data Products}{37}
