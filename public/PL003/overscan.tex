\section{Read Out Bias\label{overscan}}

Each amplifier channel has an electronic bias which varies for each pixel
value read out.  The controllers sample the bias multiple times at the end
of each line read out and record the bias values as overscan pixels.  This
results in the raw data including a set of overscan columns.  For the Mosaic
cameras there are 64 overscan columns for each amplifier image.

In processing the overscan columns are collapsed into a single column
vector.  The simple average of all columns define an {\it average overscan
vector}.  The average with the highest and lowest value excluded is called a
{\it minmax overscan vector} while a single 5-sigma clipping iteration is
call a {\it 5-sigma overscan vector}. A polynomial function fit to the
average overscan vector produces a smoothed calibration called a {\it
polynomial overscan vector}.

The pipeline electronic bias removal follows the widely used CCD reduction
model of subtracting the overscan vector from each column of the image
followed by a two dimensional bias subtraction using a dark or bias
calibration calibration image.  This section describes the overscan vector
subtraction and section \ref{zero} describes the 2D pattern subtraction.
The overscan vector then calibrates rapid electronic bias changes and drifts
between exposures, generally from amplifier temperature changes, while the
2D bias pattern, typically from voltage noise, calibrates a more static
electronic pattern.

There are five overscan subtraction options: minmax, constant, polynomial,
constant or minimax, and polynomial or minmax.  The option used is defined by a
rule in the calibration library.  Currently, the rule selects the minmax
method for all science and calibration data except for short exposures,
those less than 120 seconds, where a polynomial fit is used unless there is
evidence of rapid changes in the line bias, called {\it bias jumps}.

The "minmax" option subtracts the minmax overscan vector from each image
column.  The "constant" and "polynomial" option subtracts the polynomial
overscan vector where a 1st or 101st Legendre polynomial is used
respectively.  The constant option is mathematically the same as subtraction
the average of all bias pixels from all image pixels.

The other two options apply a constant or polynomial subtraction,
for minimizing sampling noise and low level line patterns, except when
bias jumps, abrupt and erratic changes in the bias, are detected.
When bias jumps are present the minmax subtraction is used where the
bias for each line is independent.

Bias jumps are identified using the 5-sigma overscan vector.  This vector
convolved with an "edge detection" filter of the form [-1,...,-1,1,...,1]
where the transition is in the middle.  The current filter has a half-width
of 10 pixels.  The convolved strip is checked for positive and negative
peaks. If a peak is found, a straight line is fit around the peak with the
same width as the filter.  If the reduced $\chi^2$ is larger than a fixed
(empirically determined) number, indicating the region is not a steep
gradient, it is identified as a bias jump.  For each detected jump, the
beginning and end of the points, the length, and the amplitude (both up and
down) are recorded, as are the maximum lengths and amplitudes, both up and
down. The probability that a detection is a jump is estimated, although
usually the answer is 0 or 1.

Another problem (data quality) condition detected from the overscan is
electronic drop-out.  If the sixth lowest value in the 5-sigma overscan
vector is negative or zero the image from the amplifier read out is flagged
as bad.  Often this condition consists of the entire read out having zero
values.  The exclusion of a small number of low values is to accept cases
where the initial lines have problems but the majority of the data is
usable.  When an image is flagged as bad it still processed but excluded
from some algorithms such as stacking or resampling.

After the overscan columns have been used for bias analysis and
subtraction the image is trimmed to the useful science pixels.  The
bias columns and trim region are set by the data acquisition system.
It would be a simple extension to obtain this information from the
calibration library if a need arises to override the data acquisition
system values.

When dual amplifier are read out from a CCD, resulting in two images
with their own overscans, they are merged into a single CCD image
after the overscan bias subtraction.  From then on the pipeline
operates on CCDs rather than amplifiers.

The core task which implements the overscan subtraction and trimming
is ccdtool (a version of ccdproc) and the merging is performed by
combine, both from the mscred package.
