\section{Astrometric Transformations\label{rsp}}

The images have thus far been flux and astrometrically calibrated with their
original pixel sampling and an exposure has been kept as a set of separate
images.  The set of flux calibrated images, packaged as a multi-extension
file, is one of the data products of the pipeline.  However, the pixel
sampling on the sky is complex and, for a number of reasons including
producing a simple mosaicked image data product and combining overlapping
exposures, it is desirable to transform the set of images to simple standard
sampling.

The stacking of exposures could bundle the registration, resampling, and
combining of data from a collection of single flux calibrated mosaic images
into a single operation.  However, for high performance the pipeline
separates the process into multiple steps.  Each image is first transformed
independently allowing the operation maximally parallel and distributed.
From the transformed pieces the two data products of a single mosaicked
image per exposure and a stacked image from overlapping exposures can be
created by shifting, matching flux scales, and combining without the
complexities of image interpolation.

The key to allowing simple mosaicking and stacking after
transformation is to chose a sampling grid on the sky that is common
to all images which may be combined.  Therefore, there are two
algorithmic steps, defining the grids and resampling the pixels.

This step and all steps using the transformed image require successful
flux and astrometric calibrations.  When the calibration failed there will be
no contribution to 
the resampled data products or contribution to stacks for the exposure.
Some flux calibration failures may affect only some CCDs so the
mosaicked image for the exposure will have sections missing.  An
astrometric calibration failure affects the entire exposure and so
there will be no mosaicked data product.

\subsection{Setting a Common Sampling\label{rsptan}}

The tangent points of all exposures in a "day" dataset are used to
define groups of images which will share the same sampling grid.  Note
that the tangent points for all CCDs from a common exposure are the
same so all one may think fo this step as applying to the "ftr"
dataset though the computations are done per CCD group.

At this time stacking of multiple exposures is limited to those which
are in the same "ftr" dataset.  This mainly means dither sequences
taken on the same night.  When an ftr dataset spans multiple nights
due to a small number of exposures in a night then dither sequences
and stacking of overlapping exposures may also span multiple nights.

The first step is grouping the dataset by overlaps.  This uses a nearest
neighbor clustering algorithm.  The coordinates are first sorted by
declination and gap between consecutive declination values greater than 15
arc minutes delimit declination groups.  Each coordinates in each
declination group are then sorted by right ascension and gaps in angular
separation greater than 15 arc seconds delimit final overlap groups.
Special steps are taken to handle the wrap-around discontinuity in right
ascension.

The second step is to assign each overlap group to the same tangent
point.  This is done using the coordinates of the first image in a
group and finding the nearest point on a grid of the celestial sphere.
The grid has points with approximately one degree separations in right
ascension and declination.  The precise details of the grid are not
critical as long as it is well defined.

In addition to a tangent point the sampling requires an orientation
and scale.  The most common "standard" orientation for astronomical
images is declination increase with the line index and have
right ascension decrease with column index with mimimal cross-terms.
Visually, if the image is displayed in raster order with the origin
at the lower-left of the display then north is up and east is to the
left.  The pixel interval is set to a nice number for the data.  For
the Mosaic Camera this is 0.25 arc seconds per pixel.  The final piece
of the sampling is the projection from a rectangular grid to the sky.
For optical astronomy the most common projection is the FITS "tan"
projection [REF].

The idea behind the use of a grid of tangent points is that for data
from different epochs resampled images or stacks which are nearby have
a significant chance of being sampled with the same tangent point and,
thus, can be stacked without additional interpolation.  This is not
perfect because at the border between grid points nearby images may be
split between two grid points.  This drives the grid points to be
widely spaced.  On the other hand the further a tangent point is from
the center of a field the more the image departs from having columns
aligned with north.

\subsection{Resampling Images\label{rsprsp}}

The transformation of an image from one pixel sampling to another
requires rebinning or interpolation of the data.  There are various

The pipeline defines the flux of each pixel in the output grid by
interpolating from the original image raster.  The WCS is used to compute
the point in the input image corresponding to the desired output pixel
center.  The nput pixels around that point are used to define an
interpolation function which is then evaluated at that requested point.
There are many interpolation functions that can be used including sinc,
polynomial, and "drizzling".  The pipeline has an operator defineable
configuration parameter for changing this but the recommended inerpolation
is a truncated sinc interpolation with a tabulated kernel (for computation
efficiency) of 17 pixels.

With any interpolation and complex mappings, such as from optical
distortions, there is the problem of interpolation at the edges of the
image data.  The pipeline uses a reflection boundary condition which
effectively eliminates ringing at the edges.
However, the setting of the output image size is based on a trimmed
version of the input.  The trim is eight pixels which is appropriate
for the sync interpolation.  The effect of this is that no output
pixel will map to an input pixel closer than eight pixels from the
edge, thus avoiding needing to apply boundary extension to the input
data.  While this results in a slightly smaller output image it
eliminates potential artificial edge effects.  For later stacking of
dithered exposures the trimming is like having slightly larger gaps in
the individual mosaic exposures.

Sinc interpolation is one of the more compute intensive functions but it has
the property of minimizing correlations in the initially uncorrelated noise.
A disadvanatage of this interpolation is the ringing from undersampled
features, namely cosmic rays and detector defects.  The algorithm does not
have the option to exclude bad pixels from the interpolation function which
is an important computational reason why pixels replacement is important
beyond just cosmetic appeal.

The final transformed image is stored in an image raster that is just
big enough to include all the pixels with science data.  The point is
that one does not need to pad the images to some large common
"canvas".  Defining the final image when mosaicking and stacking is
left to later with the knowledge that only integer shifts along
columns and lines is needed to align data which have the same
resampling grid.

The exposure mask is also transformed.  The mask values are not
propagated and the assumption is that any pixel flagged in the
exposure mask should be flagged in the transformed mask.  The input
mask is converted to a boolean mask.  Because mask values are integers
the non-zero value is set to 10,000 for a reason that will become
apparent,  The mask is resampled using the same transformation but
using bi-linear interpolation.  The use of linear interpolation is to
minimize ringing effects and so that the interpolated value at an
output pixel can be interpreted as the degree of contribution from a
bad pixel.  The output mask values will be integers between zero and
10,000.  So any interpolated value whose floating point value is less
than one will become zero which means, in a sense, that the contribution of
a bad pixel to the output is less than 0.01\%.  After the reampling
the non-zero mask values are set to one to make a final boolean mask.

The resampling transformation is performed by the IRAF task mscimage
from the MSCRED package.  Further details may be found in the
documentation for this task.

\section{Mosaicking\label{mosaic}}

A mosaic image is a simple raster image where all pieces of an
exposure are put together with a common world coordinate system.
Pixels where there is no data because of gaps between the CCDs and
because of distortions, rotations, alignments, and celestial projects
are handled by setting the values to a defined blank value and
recording them in a pixel mask.

Starting with the individual resampled CCDs which have been
transformed to a common coordinate grid is a simple matter.  First
no adjustments are needed for the pixel values because the flux
calibration is assumed to have brought all pixels to the same
scale and effective exposure.  Second since there is no overlap in the
original CCD geometry there is no need to worry about combining
overlapping pixels.  So to make a mosaic simply requires determining
the integer shifts for the origins of each resampled CCD image,
determining an output size that just fits all the data, and populating
the output pixels.  At the same time a mask for the output mosaic is
created and marked where there are no pixels.  The mask also is filled
with the bad pixels identified for the resampled image from the
original flux calibration.

The task with does this is imcombine.

