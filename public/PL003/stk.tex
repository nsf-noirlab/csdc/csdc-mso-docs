\section{Overlap Stacking\label{stk}}

The pipeline combines overlapping exposures when it identifies them.  They
must be within a signle "ftr" dataset since that is what is available at one
time within the pipeline data flow.  Currently, the identification of
overlapping exposures is limited to dither sequences.  These are sets of
exposures taken specifically by the observer, using a special observing
command, for filling in the mosaic gaps and detector defects.

There are three possible approaches to selecting data to combine.  One
is to stack the mosaic exposures described in \S\ref{mosaic}.  Another
is to stack all the individual resampled CCDs at one time.  The last,
and the one implemented by the pipeline, is to first stack all
resampled images from a single CCD and then stack those into the final
product. 

he pipeline uses the last approach because it fits well
with the way the pipeline parallelizes and distributes data in our
high performance system.  It is the case that the resampling steps
(\S\ref{rsp}) are distributed by CCD in "day" datasets.  In that stage
overlapping data, which includes dither sequences, are identified and
the resampling done to a common tangent point.  It is a simple matter
to follow this with a step that takes the dither sets from each "day"
dataset, makes subset "stk" dither datasets, and stacks them.  If
desired, the "stk" datasets may be operated on in parallel and
distributed.

The second step of combing the "stk" datasets is done in the same
manner as for the mosaics of individual exposures except now the
stacked images for each CCD will overlap.  Combining the CCD
stacked images for a final field stack is equivalent to either
stacking the mosaicked images for an exposure or stacking all the
individual resampled CCD images provided exposure masks are maintained
and used.

\subsection{Missing vs Bad Data}

A goal of dither stacks, besides the obvious one of deeper imaging, is to
fill in detector defects and missing data, such as from the gaps in mosaic
instruments.  This unobserved or bad data is fixed in the focal plane and so
dithering on the sky fills them in during stacking.

A related reason for dither stacks, sometimes not really dithers but
split exposures, is to handle pixels compromised by cosmic rays or
satellite trails.

Dither stacking depends on registering exposures on the sky.  In many cases,
particularly with wide-field cameras and mosaics, this requires resampling
of each exposure to a common pixel grid.  This inevitably introduces another
type of missing data at the edges of the images due to the storing of data
as rectangular image rasters.  As with the gaps, this type of missing data
is also tied to the focal plane and filled in by dithering except at the
extremes of the dither field.

However, another kind of bad data follows the sky.  This is usually related
to bright sources causing saturation and, for CCDs, "bleed trails".  Dither
stacks generally cannot fill this in except through a strategy of variable
exposure times, which is infrequently done though the pipeline handles this
case.

The way these absent, bad

The Mosaic pipeline includes algorithms using static detector defect
masks, a WCS describing the geometry of the mosaic (which implicitly
define the gaps), resampling to an undistorted pixel grid, and
identify and flag saturated and bleed trail pixels.  For cosmetic and
software reasons pixels suffering from detector defects and bright source
effects are replaced by "reasonable" values using interpolation.  These
substituted values are identified as non-science data in associated masks.

A fuzzy situation is when data is of marginal quality.
An example of this is with IR detectors where non-linearity
calibrations get gradually worse (meaning progressively less
accurate).  One wants to avoid setting a hard division between good and
bad.  In this case the data is not replaced by artificial data but is
still flagged.  Like saturation these pixels will be fixed to the sky near
bright sources.

This discussion leads to a categorization of bad pixels in stacked images,
defined as where there is no contributions from good (calibrated) pixels,
into those where the sky was not observed and those where the observations
are of poor quality or compromised.  For simplicity we call the two types
"no data" and "bad" pixels.

The pipeline
produces bad pixel masks for the stacked images which discriminate
these two types.  It also allows the stacked images to also have
cosmetically cleaner pixels, avoiding sharp artifacts as well as
producing a stack of original pixels when they are of "poor quality"
but not needing to be replaced.

There are three possiblities for handling the bad stacked pixels,
setting them to a "blank" value, interpolating across the final stack
as is done with the original exposures,
or combining the input replaced pixels as if they were good data.  The
third approach has two advantages, it avoids another pipeline step
and it randomizes the pixels somewhat to reduce the "zipper" pattern
of simple linear interpolation.  It also results in possibly
interesting pixel values for the input bad pixels which were of
"poor quality" but not needing to be replaced.

Figures X, Y, and Z show the distinction between treating the two
categories the same, producing output blank values, and differently,
producing blank values when there is no data and combined input pixel
values for the bad data.  The pipeline stacks data using the latter
approach.

\begin{figure}[h]
\begin{center}
\includegraphics[width=.45\columnwidth]{Figs/stk1a.eps}
\hfil
\includegraphics[width=.45\columnwidth]{Figs/stk1b.eps}
\end{center}
\caption{
Large region of a Mosaic dither stack using masktype="goodvalue"
showing how bad pixels associated with the bright stars result in
no overlap data:
a) output image has blank values of zero,
b) output mask has values of 1 overlayed as red.
The "ticks" at the left and bottom are incomplete filling of the gaps
by the dither.
}
\end{figure}

\begin{figure}[h]
\begin{center}
\includegraphics[width=.45\columnwidth]{Figs/stk2a.eps}
\hfil
\includegraphics[width=.45\columnwidth]{Figs/stk2b.eps}
\end{center}
\caption{
The same stack as the previous figure but using masktype="novalue":
a) output image with pixels filled in by combined interpolated values,
b) overlaid output mask with values of 1 (red) for no data and
2 (green) for interpolated values.
}
\end{figure}

\begin{figure}[h]
\begin{center}
\includegraphics[width=.45\columnwidth]{Figs/stk3a.eps}
\hfil
\includegraphics[width=.45\columnwidth]{Figs/stk3b.eps}
\end{center}
\caption{
A blow-up of previous figure showing edge overlap effects and the
remnant of the bleed trail when one input exposure did not have the
bright star and, hence, no trail.  The exposure mask identifies this
region of low significance.
}
\end{figure}

The main application used in the stacking pipeline is IRAF's
IMCOMBINE.
