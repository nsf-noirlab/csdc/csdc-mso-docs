\documentclass{../latex/sdm_doc}
\usepackage{graphicx,url,natbib,alltt,epstopdf}

\title{Calibrating Astronomical Exposures Contaminated by
Out-of-Focus (Pupil) Camera Reflections}
\author{F. Valdes$^1$}
\otheraffiliation{
$^1$NOAO Science Data Management, P.O. Box 26732, Tucson, AZ 85732}
\pubhistory{Draft: \today}
\reportnumber{PL301}
\keywords{optics, calibration, cameras}
\runningtitle{Correcting Pupil Pattern Reflections}

\begin{document}
\frontmatter

\tableofcontents
%\newpage 

\listoffigures
%\newpage

%\listoftables
%\newpage

\mainbody

%\begin{abstract}
%\end{abstract}

%\section*{Purpose of this Document}
%\addcontentsline{toc}{section}{Purpose of this Document}

%\newpage

\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}

A problem that occurs in many astronomical cameras is a double reflection
that superposes a weak, highly defocused pattern on an exposure.  The double
reflection occurs between various optical elements (e.g. filter(s), the
dewar window, corrector(s)) or the detector surface itself.  It is generally
weak (1\%-10\%) because, naturally, every effort is made to avoid
reflections through coatings, alignments, etc.  This pattern is commonly
refered to as a pupil pattern or pupil ghost.  Removing this pattern in the
presence of detector pixel response variations of comparable magnitude in
complex, mosaic detector arrays is one of the most challenging calibration
problems for such cameras.  Compounding the challenge is a confusion over
the calibration model.  This paper provides a mathematical description of
the calibration model and then provides some examples of reflection patterns
in several cameras and some algorithms used or investigated for removing the
pattern.

\section{The Calibration Model}

The imaging model is represented as

\begin{equation}
O_i^j = (I_i + I_b (1 + \alpha p_i) + \beta f_i) r_i^j \label{O}
\end{equation}

\noindent where $O_i^j$ are observed (raw) counts at position $i$ in
detector $j$, $I_i$ and $I_b$ are the source signal, $p_i$ is the reflection
pattern with an amplitude $\alpha$ relative to $I_b$, $f_i$ is a
fringe pattern with an amplitude $\beta$ and $r_i^j$ are the
pixel responses.  The separation of the source signal into variable, $I_i$,
and fixed, $I_b$ components is a representational choice that is useful to
interpret the calibration model derived in this section.  Implicit in this
model is that terms are bandpass dependent and pixel area variations are
merged into responses.  Typically $\beta$ is zero except in a few
redder filters.

In this model the source and patterns are considered independent of the
detector and are decomposed into a spatial pattern with an exposure
dependent amplitude.  The amplitudes are further represented as something
that scales with some measure of the light, $I_b$, though there is no
requirement that $\alpha$ be interpreted as a fixed reflection coefficient
nor that $\beta$ actually depend on the broad-band light.  Typically the
fringe pattern amplitude depends on the strength of the narrow night sky
lines in the bandpass but making it relative to $I_b$ does affect the
generality.  We make the assumption that the shapes of the patterns don't
change with exposure since they are caused by a very out of focus image of
the field for the reflection pattern and the diffuse night sky lines.


For clarity in the next part of the derivation we drop the $i$ and $j$
indices.  Next we write the imaging model for an on-sky exposure $S$ to be
calibrated with a flat field exposure $F$.  Note that here we assume the
data have been already corrected for instrumental biases (i.e. electronic
and dark biases).  A flat field is generally a master calibration created
from a number of individual dome flat field exposures.

\begin{eqnarray}
S & = & (I_s + I_b (1 + \alpha p + \beta f)) r_s \\
F & = & (1 + p) r_f
\end{eqnarray}

\noindent Note that the pixel responses are not assumed to be the same due
to differences in the source spectrum and, possibly, illumination of the
telescope.  For the flat field we assume the true source is uniform ($I +
I_b = I_f = $ constant), which is the definition of a flat field, and we can
assume a global normalization to avoid carrying this constant around.
Also the dome flat is assumed not to produce fringing in the detector.  If
one wants to normalize each detector in a mosaic camera separately, the
relative normalization values can be folded into the $r_f$ which then are
reflected in the illumination function as defined below.

We also define $\alpha p$ as being just $p$ in the flat field.  Then
the $\alpha I_b p$ factor is the relative scaling from the flat field to the
sky exposure.  We define the following quantities which appear later.

\begin{eqnarray}
R &\equiv& r_s / r_f \label{R} \\
L &\equiv& R / (1 + p) \label{L} \\
F' &\equiv& F L \label{F'} \\
F'' &\equiv& F - p r_f \label{F''}
\end{eqnarray}

The first, $R$, represents the difference in pixel responses between the
dome flat field and the sky exposure.  This quantity is often called the
"illumination" pattern and is typically derived as a smooth spatial function
over an amplifier but with discrete jumps between amplifiers and detector
arrays.  The quantity $L$ will become apparent below as the gain correction
due to the reflection pattern, the $(1 + p)$ term, and the illumination
response, the $R$ term.  The last quantities, $F'$ and $F''$, are two ways
of modifying the dome flat field to account for the reflection pattern.

Rearranging terms produces the following calibration model.

\begin{equation}
\frac{S}{F'} = \frac{S}{F''} = I_s + I_b + \alpha I_b p + \beta I_b f \label{calmodels}
\end{equation}

\noindent This result demonstrates several things.  First is that we get the
true sky signal, $I_s + I_b$ plus the additive reflection patterns by
correcting the observed dome flat field.  Second the debate over whether one
subtracts the reflected light pattern from the flat field or "flat fields"
the flat is resolved by both yielding equivalent results.  So the choice is
then about which correction is easier to determine.  At this point the
pattern scaling term $I_b$ can be interpreted as the (mean) sky background.
If the cumulative light of sources and/or light from outside of the field of
view contributes to the strength of the reflection pattern this is accounted
for by the exposure dependent amplitude term $\alpha_s$.

Now consider what happens if the dome flat is used without a correction.
Writing that operation produces

\begin{equation}
\frac{S}{F} = L I_s + R I_b \frac{1 + \alpha p}{1 + p} + R I_b
\frac{\beta f}{1 + p} \label{stdcalmodel}
\end{equation}

\noindent First we see that the sky signal has a spatially varying gain
calibration error given by $L$.  Second, if $\alpha$ is nearly one the
reflection pattern disappears in the flat fielded data.  Thus one might be
led to \emph{incorrectly} believe, by visual inspection, that the data have
been well flat fielded.  This near equality has been seen in some (but not
all) instruments which is why we demonstrate this result.  Finally, in
(\ref{stdcalmodel}) we can easily see that in the absence of a reflection
pattern, where $p = 0$ and $L = R$, the standard calibration model of
applying a dome flat field, an illumination correction, and
subtracting a fringe pattern is obtained.

The calibration model that we recommend is that using $F'$ where the
dome flat field is corrected by "flat fielding" with a normalized
pattern.  This is shown explicitly below.

\begin{equation}
I_s + I_b = \frac{S - \alpha S_b p}{F L} - \beta I_b f = \frac{S}{F L}
- \alpha I_b p - \beta I_b f
\label{calmodel}
\end{equation}

\noindent where $I_b \equiv S_b / (F L)$.  The two forms on the right differ
by whether one subtracts the reflection pattern in the raw or in the flat
fielded data.  One could also just leave the pattern in the flat fielded
data as part of the background.  However, if the pattern is sufficiently
strong, leaving it can cause other processing problems such as in
stacking exposures.

There are three independent quantities in the calibration model relating to
the reflection pattern; $R$, $p$, and $\alpha$.  For gain calibration only
the combination of two, namely $L = R / (1 + p)$, is needed.  For fringe
subtraction the quantities $\beta$ and $f$ are, obviously, also needed.

\section{Calibration Algorithms}

Teasing out the various quantities for the calibration model is an
approximation process because of trade-offs between coupled terms that are
often impossible to cleanly separate.  This sometimes involves special types of
exposures and sometimes iterative removal starting with the strongest terms
first.  In the examples below the difference in approaches are the result
of the differences in strength of the reflection (i.e. $\alpha$),
the variation in the detector responses, and the size and diffuseness
of the pupil pattern.

\subsection{Dark Energy Camera}

The Dark Energy Camera (DECam) at the Cerro Tololo Inter-American
Observatory (CTIO) Blanco telescope has a large pattern in some filters (see
figure~\ref{DECam1}).  This has the challenges that it spans many CCDs and,
with the detector response variations, is difficult identify
in dome flat fields.  While the pattern is not clearly visible in dome flat
fields it can easily be seen in dark sky flat field stacks, even with just
dome flat calibrated exposures, which tells us that $\alpha$
is not near one in the calibration model.  It can also be seen in
photometric "star flats" as discussed below.

\begin{figure}[h]
\begin{center}
\includegraphics[height=2in]{DECam.eps}
\end{center}
\caption{NOAO DECam - Dark sky stacks in u (left) and z (right). The g
filter also has a comparable pattern but not other filters.  The data was
calibrated just with dome flat fields which were not gain corrected.  In
addition to illustrating the size and amplitude of the pattern it also
demonstrates that the amplitude in dome flats is not such as to "flat field"
the pattern away.  The bright spot at the center of the u filter is a
different effect which is still not well understood.
\label{DECam1}}
\end{figure}

The use of star flats attacks the problem directly through the calibration
model.  Since it is based on local background subtracted photometry,
symbolized by $S_i^{\star} \equiv \mathrm{phot} [S_i / F_i]$ for a source at
position $i$, the background term in (\ref{calmodel}) is eliminated.
Now consider taking many dithered exposures of a fairly dense field
in photometric conditions.  Identify all instances of the same star by
the label $j$ so that $S_{ij}^\star$ is the photometric flux of star
$j$ at position $i$.  With enough stars per exposure and large enough
dithers so that $i$ for a particular star samples significantly different
parts of the field the function $L$ can be determined by

\begin{equation}
\min_{ij} \left [ \left ( A_i S_{ij}^\star -
\overline{A_i S_{ij}^\star} \right )^2 \right ] \label{starflat}
\end{equation}

\noindent where $\min$ means to minimize over all photometric values and the
mean is over all instances of star $j$ and $A \equiv 1 / L$ (defined for
convenience). The separation of $A$ from the aperture photometry assumes
that it is effectively constant over the aperture.  Note that even if
conditions are not perfectly photometric, one can adjust by the relative zero
points derived from all common stars or matches to magnitudes in a reference
catalog.  Equation (\ref{starflat}) is in terms of linear fluxes but it
could also be cast and solved in terms of instrumental magnitudes if
desired.

There are currently two methods being used for solving (\ref{starflat}).
One is based on dividing up $A_i$ into cells (also called "super pixels")
and not using any assumption other than a constant value per cell.  The cell
sizes are 512 x 512 (which is coincidentally the about the same as ODI OTA
cells discussed elsewhere).  The other method is a functional approach.
The cell method is currently in use with the NOAO Community Pipeline.

A point to note is that even in the absence of a reflection, the star flat
method still produces a gain calibration function which is purely due to the
illumination correction $R$.  In the presence of a reflection some
assumptions on the shape of the pattern, i.e., a smooth donut-shaped pupil
pattern, would be needed to separate illumination contributions from the
reflection component.  However, for the purposes of the photometric gain
calibration the function $A$ is sufficient in and of itself.

Since the derivation of the gain calibration $A$ depends on specific
observational data, it is used in the NOAO Community Pipeline as a
static external calibration.  The pipeline does not apply the pupil
subtraction since, as just noted, it is not possible to cleanly separate the
reflection pattern from the illumination corrections with just the
star flat derived $A$ function nor select the amplitude factor
$\alpha$.

\subsubsection*{Relation to the DES Calibration Model}

The DES calibration model is nearly identical except for defining

\begin{eqnarray}
p_{des} &=& p r_f \\
\alpha &=& 1
\end{eqnarray}

\noindent and not explicitly using separate response terms for the dome flat
field and the sky. However, the illumination function $R$ is implicit
in the star flat gain calibration.

\subsection{Mosaic Imager}

The NOAO Mosaic Imager (MOSAIC) at the Kitt Peak National Observatory (KPNO)
Mayall telescope has a pronounced pupil pattern in a number of filters.
Figure \ref{Mosaic1} shows an example of the pattern in a raw exposure and
after it has been removed as described in this section.  The pattern covers
4 CCDs and 4 or 8 amplifier images depending on readout mode.  The pattern
is sufficiently strong and stable that the center and edges of the pattern
can be well determined and used in the calibration algorithms.  It is also
useful that the pattern does not cover all of any CCD.

\begin{figure}[h]
\begin{center}
\includegraphics[height=2in]{mosaic.eps}
\end{center}
\caption{NOAO MOSAIC - On the left is a raw exposure (Bw filter).  A dome
would be similar without the sources.  In the middle is the calibrated
version after a pattern corrected dome flat was applied and the background
pattern removed.  On the left is the pattern extracted from a dark sky
stack of a number of exposures.
\label{Mosaic1}}
\end{figure}

The calibration model (\ref{calmodel}) is rewritten as

\begin{eqnarray}
F_c &\equiv& F / (1 + p) \\
I &=& \frac{(S/F_c - \alpha p' - \beta f)}{R}
\end{eqnarray}

This shows the calibration as broken up into the steps of 1) correct the
dome flat field, 2) apply the corrected flat field to the sky observation,
3) subtract a reflection pattern background, 4) subtract a fringe pattern,
and 5) apply an illumination correction.  Note that for the CCDs which are
not affected by the pattern only the fourth step is needed and so the other
steps are restricted to just the affected CCDs.

The first step is to find $1 + p$ from the dome flat field.  As seen in
fig.~\ref{Mosaic1} there are plenty of pixels not affected by the
pattern, so we normalize each CCD by its mean over the unaffected
areas.  For dual-amp data the normalization is done by amplifier. This largely
normalizes the dome response leaving a good view of the pattern.  The
pattern is then fit over all the affected CCDs simultaneously with radial
and azimuthal functions.  The knowledge of the inner and out edges of the
pattern allows fitting an azimuthal background.  The result is a model
pattern $p$.  Finally the pattern is removed from the dome flat field by
adding 1, removing the normalizations, and dividing into the dome flat
field.  Note that because we fit a smooth function across the full ring in
the four normalized CCDs, the flat field responses $r_f$ are washed away.

For removing the background pattern from the on-sky exposures, a dark sky
stack is created from a set of observerations over one or more nights.  The
stack makes use of object detection masks as well as statistical clipping
and various heuristics to remove exposures with large or very crowded
sources and bad sky conditions (transparency and twilight contamination).
The pattern, $p'$, is "scaped" off the stack using knowledge of the position
and edges of the pattern to fit an a azimuthal background function.  The
pattern amplitudes, $\alpha$, are determined for each exposure.  Again it is
very important to use object detection masks to deal with sources that fall
in the pattern. Also weights based on the pattern are used.  Finally, the
scaled pattern, $\alpha p'$, is subtracted from the exposure.  A note here
is that the reflection pattern is derived in two different ways at two
different times.  This means the pattern is not required to be exactly the
same in both the dome flat field and the sky exposures, hence denoting the
pattern as $p'$.

If a pattern $p'$ for the particular set of nights is obtained it is
archived.  If the set of exposures is not suitable for deriving the pattern
then an earlier archived pattern is used.

The flat field correction described in the first step works perfectly in all
cases.  The background subtraction from the science exposures, however, can
be problematic for some data because of the sources in the pattern.  The
automatic scaling algorithm works most of the time but there are cases that
the eye can see as over or under subtracted.  An individual could improve
things with a more tedious trial-and-error subtraction and display.

If there is fringing in the particular filter the fringe pattern, $f$,
is found by making a dark sky stack and subtracting a low-pass
filtered version of the stack.  The amplitude, $\beta$, is determined
for each exposure with object masking and weighting to the regions
where the fringe pattern amplitude is stronger.

After subtraction of the reflection and fringe pattern from all exposures,
each with different scales $\alpha$ and $\beta$, is completed then the
illumination correction $R$ is derived in a subsequent creation of a dark
sky stack.  Again we make use of object masking and statistical rejection to
remove the effects of sources.  As with deriving the reflection pattern, it
is not always possible to obtain an illumination correction for a dataset.
In this case an earlier archived illumination correction is used.  The
middle panel of figure~\ref{Mosaic1} is after all the steps.

\subsection{One Degree Imager}

The One Degree Imager (ODI) at the Wisconsin-Yale-Indiana-NOAO (WIYN)
Observatory 3.5m telescope has weak to modest strength patterns (see
figure~\ref{ODI1}).  These present some unusual challenges.  The camera has
three "layers" of filters (with only one used at a time) which, because of the
differing distances, produce pupil patterns of differing sizees and
diffuseness.  The filters are currently not exactly repositioned into
the beam which introduces a small amount of shift in the pattern
relative to the focal plane.  The camera is mounted on an alt-az telescope
so the source field rotates.  Finally, the detectors are orthogonal transfer
arrays (OTAs) where each OTA is electronically divided into 64 cells; i.e.
there are 64 amplifiers per OTA and multiple OTAs in a mosaic.

\begin{figure}[h]
\begin{center}
\includegraphics[height=2in]{g_pupil.eps}
\end{center}
\caption{WIYN ODI - The pupil pattern seen in the 4 central OTAs of
ODI.  This was extracted using the ratio of g-band exposures between the
stronger layer and the weaker, more diffuse layer.  There are two bad
cells visible.
\label{ODI1}}
\end{figure}

Because of the large number of small cells the challenge with separating the
response term, $R$, and even seeing the pattern in dome flats, is large.

The algorithms to address the reflection in ODI are still evolving.  The
approaches currently are based on obtaining a pattern template for which the
amplitude factors, $\alpha_f$ and $\alpha_s$, are then determined.  This is
the same algorithm as used successful with the NOAO MOSAIC camera.

The challenge is in isolating the pattern.  Several interesting approaches
have been explored for "flat fielding" the cell gains independent of the
pattern.  One method is to use dome flats taken in a filter layer that has a
fainter pattern as a reference flat field for dome flats in the layer with
the stronger pattern.  This has been tried both with the same filter and
with different filters in the reference layer.  For one combination of the
same filter in different layers this works fairly well (figure~\ref{ODI1}).

Another very promising approach is to use a very broad band filter (actually
just clear glass) to produce the reflection and then flat field it with an
exposure that has no filter in the optical path.  The problem with this is
getting a (cheap) clear glass insert that doesn't smear out the structure
relative to that in the optical quality filters.

A star flat approach has not been tried.  The challenge for this method is
to make sure every cell is visited.  But in a sense this is much like the
"super pixel" cell method used to solve (\ref{starflat}) for DECam.  The
DECam cells are nearly the same size as ODI cells.  On the other hand the
plate scale is higher in ODI which would require even denser fields in good
seeing conditions to achieve similar sampling.

%\newpage

% Use bib files if possible.
%\bibliography{PLSeries}
%\bibliographystyle{abbrv}

% Otherwise use:
%\begin{thebibliography}{}
%\bibitem[Author(DATE)]{REF} Author, A., et al., DATE, PUB, 1234, 56
%\end{thebibliography} 
% Include the following to make the References appear in the TOC.
%\addcontentsline{toc}{section}{References}

\end{document}
